package com.layanan.kesiswaan.mdta.utils;

public class Constants {
    public static final String APP_NAME = "Layanan-Kesiswaan-MDTA";
    public static final String BASE_API_URL = "https://layanan-kesiswaan-mdta.herokuapp.com";

    public static final String LOGIN_PATH = "/login";
    public static final String GRADES_PATH = "/grades";
    public static final String USER_PATH = "/user";
    public static final String ATTENDANCE_PATH = "/student-attendance";
    public static final String ADMINISTRATION_PATH = "/administration";

//    public static final String GET = "GET", POST = "POST", DELETE = "DELETE", PATCH = "PATCH";
    public static final String COMMON_ERROR_MESSAGE = "Oops, something went wrong. Please try again";

    public static final String CLASSROOM_ID = "classroomId";
    public static final String SEMESTER = "semester";
    public static final String DATE = "date";

    public static final String INTENT_DATA = "intentData";
    public static final String INTENT_QUERY = "intentQuery";
    public static final String INTENT_TITLE = "intentTitle";

    public static final String LOADING_TEXT = "Loading...";
    public static final String DATA_NOT_FOUND = "Data not found";
    public static final String STUDENT_NOT_SET = "Please contact administrator to add your student";
}
