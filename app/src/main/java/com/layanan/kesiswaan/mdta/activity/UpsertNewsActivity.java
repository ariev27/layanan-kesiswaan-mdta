package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.NewsModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;

public class UpsertNewsActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txtTitle)
    TextInputEditText txtTitle;
    @BindView(R.id.txtDescription)
    TextInputEditText txtDescription;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    NewsModel newsModelData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_news);
        ButterKnife.bind(this);

        newsModelData = (NewsModel) getIntent().getSerializableExtra(INTENT_DATA);
        if (!isEmpty(newsModelData)) {
            loadData(newsModelData, progressDialog);
        }
        btnSubmit.setOnClickListener(v -> upsertNews(ctx, progressDialog));
    }

    public void loadData(NewsModel newsModel, ProgressDialog progressDialog) {
        progressDialog.show();
        txtTitle.setText(newsModel.getTitle());
        txtDescription.setText(newsModel.getDescription());
        progressDialog.dismiss();
    }

    private void upsertNews(Context ctx, ProgressDialog progressDialog) {
        txtTitle.setError(null);
        txtDescription.setError(null);

        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageTitle = String.format("%s %s", "Title", cannot_empty);
        String errMessageDescription = String.format("%s %s", "Description", cannot_empty);
        if (isEmpty(txtTitle)) {
            txtTitle.setError(errMessageTitle);
            focusView = txtTitle;
            cancel = true;
        } else if (isEmpty(txtDescription)) {
            txtDescription.setError(errMessageDescription);
            focusView = txtDescription;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody;
            if (newsModelData == null) {
                requestBody = new FormBody.Builder()
                        .add("title", txtTitle.getText().toString())
                        .add("description", txtDescription.getText().toString())
                        .build();
            } else {
                requestBody = new FormBody.Builder()
                        .add("id", newsModelData.getId())
                        .add("title", txtTitle.getText().toString())
                        .add("description", txtDescription.getText().toString())
                        .build();
            }

            String params = RequestHelper.GenerateApiRequestParam(Features.NEWS);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }
}