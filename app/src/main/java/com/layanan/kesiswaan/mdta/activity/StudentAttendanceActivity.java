package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.StudentModel;
import com.layanan.kesiswaan.mdta.models.StudentAttendanceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT_ATTENDANCE;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.CLASSROOM_ID;
import static com.layanan.kesiswaan.mdta.utils.Constants.DATA_NOT_FOUND;
import static com.layanan.kesiswaan.mdta.utils.Constants.DATE;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;

public class StudentAttendanceActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.tableView)
    TableLayout tableLayout;
    @BindView(R.id.swLayout)
    SwipeRefreshLayout swLayout;

    @BindView(R.id.fabAttendance)
    FloatingActionButton fabAttendance;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
            TableLayout.LayoutParams.WRAP_CONTENT);
    String classroomId, date;
    String attendancesQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_attendance);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ButterKnife.bind(this);

        if (sessionManager.isLoggedIn()) {
            fabAttendance.setVisibility(View.VISIBLE);
        }

        fabAttendance.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertAttendanceActivity.class)));

        classroomId = getIntent().getStringExtra(CLASSROOM_ID);
        date = getIntent().getStringExtra(DATE);
        attendancesQuery = getIntent().getStringExtra(INTENT_QUERY);
        tableLayout.setStretchAllColumns(true);

        String title = String.format("%s %s %s %s %s", getResources().getString(R.string.attendanceList), "KELAS", classroomId, "TANGGAL", date);
        tvTitle.setText(title);

        loadData(attendancesQuery, progressDialog);

        swLayout.setOnRefreshListener(() -> new Handler().postDelayed(() -> {
            loadData(attendancesQuery, progressDialog);
            swLayout.setRefreshing(false);
        }, 2000));
    }

    public void loadData(String attendanceQuery, ProgressDialog progressDialog) {
        Request request = RequestHelper.BuildRequest(GET, attendancesQuery);
        progressDialog.show();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                String errMessage = e.getMessage();
                loggerError(errMessage);
                Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    progressDialog.dismiss();
                    String res = response.body().string();
                    StudentAttendanceModel[] studentAttendanceModels;
                    studentAttendanceModels = mapToGradeObjectList(res);
                    renderTable(ctx, studentAttendanceModels, tableLayout, progressDialog);
                } catch (JSONException | IOException e) {
                    progressDialog.dismiss();
                    String errMessage = e.getMessage();
                    loggerError(errMessage);
                    Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private StudentAttendanceModel[] mapToGradeObjectList(String attendanceData) throws JSONException {
        JSONArray jsonArray = new JSONArray(attendanceData);
        int length = jsonArray.length();
        StudentAttendanceModel[] attendanceList = new StudentAttendanceModel[length];
        for (int i = 0; i < length; i++) {
            JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
            StudentAttendanceModel attendance = new StudentAttendanceModel();

            String id = jsonObject.getString("id");
            String classroomId = jsonObject.getString("classroomId");
            String description = jsonObject.getString("description");
            String createdAt = jsonObject.getString("createdAt");
            String updatedAt = jsonObject.getString("updatedAt");

            StudentModel studentModel = new StudentModel();
            JSONObject objStudent = jsonObject.getJSONObject("student");
            studentModel.setUsername(objStudent.getString("username"));
            studentModel.setFullName(objStudent.getString("fullName"));
            studentModel.setClassroomId(objStudent.getString("classroomId"));

            attendance.setId(id);
            attendance.setStudent(studentModel);
            attendance.setClassroomId(classroomId);
            attendance.setDescription(description);
            attendance.setCreatedAt(createdAt);
            attendance.setUpdatedAt(updatedAt);

            attendanceList[i] = attendance;
        }
        return attendanceList;
    }

    void renderTable(Context ctx, StudentAttendanceModel[] data, TableLayout tableLayout, ProgressDialog progressDialog) {
        runOnUiThread(() -> {
            trParams.setMargins(0, 0, 0, 0);
            tableLayout.removeAllViews();

            createTableHeader(ctx, tableLayout);
            createTableBody(ctx, data);
        });
    }

    void createTableHeader(Context ctx, TableLayout tableLayout) {
        int primaryColor = getResources().getColor(R.color.colorPrimary);

        TableRow trHeader = new TableRow(ctx);
        trHeader.setBackgroundColor(primaryColor);
        TextView tvHeader1 = new TextView(ctx);
        TextView tvHeader2 = new TextView(ctx);
        TextView tvHeader3 = new TextView(ctx);
        TextView tvHeader4 = new TextView(ctx);

        tvHeader1.setTextColor(Color.WHITE);
        tvHeader2.setTextColor(Color.WHITE);
        tvHeader3.setTextColor(Color.WHITE);
        tvHeader4.setTextColor(Color.WHITE);

        tvHeader1.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader2.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader3.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader4.setGravity(Gravity.CENTER_HORIZONTAL);

        tvHeader1.setPadding(10, 10, 10, 10);
        tvHeader2.setPadding(10, 10, 10, 10);
        tvHeader3.setPadding(10, 10, 10, 10);
        tvHeader4.setPadding(10, 10, 10, 10);

        tvHeader1.setTypeface(null, Typeface.BOLD);
        tvHeader2.setTypeface(null, Typeface.BOLD);
        tvHeader3.setTypeface(null, Typeface.BOLD);
        tvHeader4.setTypeface(null, Typeface.BOLD);

        tvHeader1.setBackgroundColor(primaryColor);
        tvHeader2.setBackgroundColor(primaryColor);
        tvHeader3.setBackgroundColor(primaryColor);
        tvHeader4.setBackgroundColor(primaryColor);

        tvHeader1.setText(getResources().getString(R.string.no));
        tvHeader2.setText(getResources().getString(R.string.studentNameCaps));
        tvHeader3.setText(getResources().getString(R.string.classroomCaps));
        tvHeader4.setText(getResources().getString(R.string.descriptionCaps));

        trHeader.addView(tvHeader1);
        trHeader.addView(tvHeader2);
        trHeader.addView(tvHeader3);
        trHeader.addView(tvHeader4);

        tableLayout.addView(trHeader, trParams);
    }

    void createTableBody(Context ctx, StudentAttendanceModel[] attendances) {
        int primaryColor = getResources().getColor(R.color.colorPrimary);
        int primaryColorLight = getResources().getColor(R.color.colorPrimaryLight);

        if (attendances.length == 0) {
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT);
            TextView tvBody = new TextView(ctx);
            TableRow trBody = new TableRow(ctx);

            trBody.setPadding(0, 0, 0, 0);
            trBody.setLayoutParams(trParams);

            tvBody.setText(DATA_NOT_FOUND);
            tvBody.setTextColor(Color.WHITE);
            tvBody.setGravity(Gravity.CENTER_HORIZONTAL);
            tvBody.setTypeface(null, Typeface.BOLD);
            tvBody.setAllCaps(true);
            tvBody.setPadding(10, 30, 10, 30);

            trBody.setBackgroundColor(primaryColorLight);
            trBody.setGravity(Gravity.CENTER_HORIZONTAL);
            trBody.addView(tvBody);
            tableLayout.addView(trBody, trParams);
        } else {
            for (int i = 0; i < attendances.length; i++) {
                StudentAttendanceModel attendance = attendances[i];
                TextView tvBody1 = new TextView(ctx);
                TextView tvBody2 = new TextView(ctx);
                TextView tvBody3 = new TextView(ctx);
                TextView tvBody4 = new TextView(ctx);
                tvBody1.setText(String.valueOf(i + 1));
                tvBody2.setText(String.valueOf(attendance.getStudent().getFullName()));
                tvBody3.setText(attendance.getClassroomId());
                tvBody4.setText(String.valueOf(attendance.getDescription()));

                tvBody1.setTextColor(Color.WHITE);
                tvBody2.setTextColor(Color.WHITE);
                tvBody3.setTextColor(Color.WHITE);
                tvBody4.setTextColor(Color.WHITE);

                tvBody1.setBackgroundColor(primaryColor);
                tvBody2.setBackgroundColor(primaryColorLight);
                tvBody3.setBackgroundColor(primaryColor);
                tvBody4.setBackgroundColor(primaryColorLight);

                tvBody1.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody2.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody3.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody4.setGravity(Gravity.CENTER_HORIZONTAL);

                tvBody1.setPadding(10, 30, 10, 30);
                tvBody2.setPadding(10, 30, 10, 30);
                tvBody3.setPadding(10, 30, 10, 30);
                tvBody4.setPadding(10, 30, 10, 30);

                TableRow trBody = new TableRow(ctx);
                trBody.setPadding(0, 0, 0, 0);
                trBody.setLayoutParams(trParams);

                trBody.addView(tvBody1);
                trBody.addView(tvBody2);
                trBody.addView(tvBody3);
                trBody.addView(tvBody4);

                if (sessionManager.isLoggedIn()) {
                    trBody.setOnLongClickListener(v -> {
                        optionDialog(STUDENT_ATTENDANCE, String.valueOf(attendance.getId()), attendancesQuery, attendance, UpsertAttendanceActivity.class, layout);
                        return false;
                    });
                }

                tableLayout.addView(trBody, trParams);
            }
        }
    }

    @Override
    protected void onDestroy() {
        progressDialog.dismiss();
        super.onDestroy();
    }
}
