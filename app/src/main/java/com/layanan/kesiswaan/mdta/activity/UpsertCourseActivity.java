package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;

public class UpsertCourseActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txtId)
    TextInputEditText txtId;
    @BindView(R.id.txtName)
    TextInputEditText txtName;
    @BindView(R.id.txtDescription)
    TextInputEditText txtDescription;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_course);
        ButterKnife.bind(this);

        btnSubmit.setOnClickListener(v -> createCourse(ctx, progressDialog));
    }

    private void createCourse(Context ctx, ProgressDialog progressDialog) {
        txtId.setError(null);
        txtName.setError(null);
        txtDescription.setError(null);

        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageId = String.format("%s %s", "Id", cannot_empty);
        String errMessageName = String.format("%s %s", "Name", cannot_empty);
        String errMessageDescription = String.format("%s %s", "Description", cannot_empty);
        if (isEmpty(txtId)) {
            txtId.setError(errMessageId);
            focusView = txtId;
            cancel = true;
        } else if (isEmpty(txtName)) {
            txtName.setError(errMessageName);
            focusView = txtName;
            cancel = true;
        } else if (isEmpty(txtDescription)) {
            txtDescription.setError(errMessageDescription);
            focusView = txtDescription;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody = new FormBody.Builder()
                    .add("id", txtId.getText().toString())
                    .add("name", txtName.getText().toString())
                    .add("description", txtDescription.getText().toString())
                    .build();

            String params = RequestHelper.GenerateApiRequestParam(Features.COURSE);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }
}