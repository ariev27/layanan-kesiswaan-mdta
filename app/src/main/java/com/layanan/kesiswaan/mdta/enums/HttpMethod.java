package com.layanan.kesiswaan.mdta.enums;

public enum HttpMethod {
    GET, POST, DELETE, PATCH
}
