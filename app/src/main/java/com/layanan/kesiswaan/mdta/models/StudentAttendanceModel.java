package com.layanan.kesiswaan.mdta.models;

import java.io.Serializable;

public class StudentAttendanceModel implements Serializable {
    private StudentModel studentModel;
    private String id, classroomId, description, createdAt, updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StudentModel getStudent() {
        return studentModel;
    }

    public void setStudent(StudentModel studentModel) {
        this.studentModel = studentModel;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
