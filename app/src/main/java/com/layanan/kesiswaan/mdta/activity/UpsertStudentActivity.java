package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.Features.CLASSROOM;
import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UpsertStudentActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txtUsername)
    TextInputEditText txtUsername;
    @BindView(R.id.txtFullName)
    TextInputEditText txtFullName;
    @BindView(R.id.spinnerClassRoom)
    Spinner spClassroom;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_student);
        ButterKnife.bind(this);

        loadSpinnerCallApi(this, progressDialog, spClassroom, "", CLASSROOM);

        btnSubmit.setOnClickListener(v -> createStudent(ctx, progressDialog));
    }

    private void createStudent(Context ctx, ProgressDialog progressDialog) {
        txtUsername.setError(null);
        txtFullName.setError(null);
        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageUsername = String.format("%s %s", "Username", cannot_empty);
        String errMessageFullName = String.format("%s %s", "Full name", cannot_empty);
        if (isEmpty(txtUsername)) {
            txtUsername.setError(errMessageUsername);
            focusView = txtUsername;
            cancel = true;
        } else if (isEmpty(txtFullName)) {
            txtFullName.setError(errMessageFullName);
            focusView = txtFullName;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody = new FormBody.Builder()
                    .add("username", txtUsername.getText().toString())
                    .add("fullName", txtFullName.getText().toString())
                    .add("classroomId", spClassroom.getSelectedItem().toString())
                    .build();

            String params = RequestHelper.GenerateApiRequestParam(Features.STUDENT);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }

    public void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
//        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(LOADING_TEXT);

        dialog.show();
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String params = RequestHelper.GenerateRequestParam(features);
        Request request = RequestHelper.BuildRequest(GET, params);
        logger(request);
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
                String message = e.getMessage();
                loggerError(message);
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                runOnUiThread(() -> {
                    try {
                        String res = response.body().string();
                        List<String> listSpinnerData = new ArrayList<>();
                        if (response.isSuccessful()) {
                            JSONArray jsonArray = new JSONArray(res);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String id = "";
                                if (features == STUDENT) {
                                    id = jsonObject.getString("username");
                                } else {
                                    id = jsonObject.getString("id");
                                }
                                listSpinnerData.add(id);
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                                    listSpinnerData);
                            spinner.setAdapter(adapter);
                            if (selectedPosition != null)
                                spinner.setSelection(adapter.getPosition(selectedPosition));
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(res);
                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        dialog.dismiss();
                        String message = e.getMessage();
                        loggerError(message);
                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
