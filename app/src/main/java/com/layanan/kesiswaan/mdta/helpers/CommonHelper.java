package com.layanan.kesiswaan.mdta.helpers;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.activity.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;

import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.layanan.kesiswaan.mdta.utils.Constants.APP_NAME;

public class CommonHelper extends BaseActivity {

    public static boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmpty(TextInputEditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmpty(String string) {
        return string.trim().length() <= 0;
    }

    public static boolean isEmpty(Collection obj) {
        return obj == null || obj.isEmpty();
    }

    public static <T> boolean isEmpty(T obj) {
        return obj == null;
    }

    public static void logger(int message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(String message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(JSONArray message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(Response message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(ResponseBody message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(Request message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void logger(JSONObject message) {
        System.out.println(APP_NAME + " " + message);
    }

    public static void loggerError(String message) {
        System.out.println(APP_NAME + " ERROR: " + message);
    }

    public static void loggerError(IOException message) {
        System.out.println(APP_NAME + " ERROR: " + message);
    }

    public static void loggerError(JSONException message) {
        System.out.println(APP_NAME + " ERROR: " + message);
    }
//
//    public void loadUser(Context ctx, ProgressDialog dialog, AutoCompleteTextView txtFullName, TextView tvSelectedItem) {
//        dialog.setIndeterminate(true);
//        dialog.setCancelable(false);
//        dialog.setInverseBackgroundForced(false);
////        dialog.setCanceledOnTouchOutside(false);
//        dialog.setMessage(LOADING_TEXT);
//
//        dialog.show();
//        OkHttpClient httpClient = new OkHttpClient.Builder()
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
//
//        String usersParam = RequestHelper.GenerateRequestParam(Features.USERS);
//        Request usersRequest = RequestHelper.BuildRequest(GET, usersParam);
//        logger(usersRequest);
//        httpClient.newCall(usersRequest).enqueue(new okhttp3.Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                loggerError(e);
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) {
//                runOnUiThread(() -> {
//                    try {
//                        String res = response.body().string();
//                        List<String> classRoomList = new ArrayList<>();
//                        if (response.isSuccessful()) {
//                            JSONArray jsonArray = new JSONArray(res);
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                String id = jsonObject.getString("id");
//                                classRoomList.add(id);
//                            }
//                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
//                                    classRoomList);
//                            txtFullName.setAdapter(adapter);
//                            dialog.dismiss();
//                            logger("KESINI 1");
//                        } else {
//                            dialog.dismiss();
//                            JSONObject jsonObject = new JSONObject(res);
//                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (JSONException | IOException e) {
//                        dialog.dismiss();
//                        loggerError(e.getMessage());
//                    }
//                });
//            }
//        });
//    }

//    public void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
//        dialog.setIndeterminate(true);
//        dialog.setCancelable(false);
//        dialog.setInverseBackgroundForced(false);
////        dialog.setCanceledOnTouchOutside(false);
//        dialog.setMessage(LOADING_TEXT);
//
//        dialog.show();
//        OkHttpClient httpClient = new OkHttpClient.Builder()
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
//
//        String params = RequestHelper.GenerateRequestParam(features);
//        Request request = RequestHelper.BuildRequest(GET, params);
//        logger(request);
//        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                dialog.dismiss();
//                String message = e.getMessage();
//                loggerError(message);
//                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) {
//                runOnUiThread(() -> {
//                    try {
//                        String res = response.body().string();
//                        List<String> listSpinnerData = new ArrayList<>();
//                        if (response.isSuccessful()) {
//                            JSONArray jsonArray = new JSONArray(res);
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                String id = "";
//                                if (features == STUDENT) {
//                                    id = jsonObject.getString("username");
//                                } else {
//                                    id = jsonObject.getString("id");
//                                }
//                                listSpinnerData.add(id);
//                            }
//                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
//                                    listSpinnerData);
//                            spinner.setAdapter(adapter);
//                            if (selectedPosition != null)
//                                spinner.setSelection(adapter.getPosition(selectedPosition));
//                            dialog.dismiss();
//                        } else {
//                            dialog.dismiss();
//                            JSONObject jsonObject = new JSONObject(res);
//                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (JSONException | IOException e) {
//                        dialog.dismiss();
//                        String message = e.getMessage();
//                        loggerError(message);
//                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });
//    }

    public void loadSpinner(Context ctx, Spinner spinner, String[] adapterData, String selectedPosition) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                adapterData);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition(selectedPosition));
    }

    public void showDatePickerDialog(Context ctx, EditText txtDate) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            txtDate.setText(String.format("%s-%s-%s", year, month + 1, day));
        };

        new DatePickerDialog(ctx, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void showDatePickerDialog(Context ctx, TextInputEditText txtDate) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            txtDate.setText(String.format("%s-%s-%s", year, month + 1, day));
        };

        new DatePickerDialog(ctx, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
