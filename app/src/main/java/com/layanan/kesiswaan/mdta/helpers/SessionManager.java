package com.layanan.kesiswaan.mdta.helpers;

import static com.layanan.kesiswaan.mdta.utils.Constants.APP_NAME;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.layanan.kesiswaan.mdta.activity.LoginActivity;
import com.layanan.kesiswaan.mdta.models.StudentModel;


public class SessionManager {
    SharedPreferences pref;
    Editor editor;
    Context context;
    Gson gson = new Gson();
    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = APP_NAME;
    private static final String IS_LOGIN = "isLoggedIn";
    public static final String USERNAME = "username", FULL_NAME = "fullName",
            ROLE = "role", STUDENT = "student", TOKEN = "token";

    // Constructor
    public SessionManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public void createLoginSession(String username, String fullName, String role, StudentModel student, String token) {
        String studentStr = gson.toJson(student, StudentModel.class);
        editor.putString(USERNAME, username);
        editor.putString(FULL_NAME, fullName);
        editor.putString(ROLE, role);
        editor.putString(STUDENT, studentStr);
        editor.putString(TOKEN, token);
        editor.putBoolean(IS_LOGIN, true);

        editor.commit();
    }

    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // Clearing all data from Shared Preferences
            editor.clear();
            editor.commit();
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
        }

    }


    /**
     * Clear session details
     * public void logoutUser() {
     * // Clearing all data from Shared Preferences
     * editor.clear();
     * editor.commit();
     * <p>
     * <p>
     * // After logout redirect user to Loing Activity
     * Intent i = new Intent(_context, LoginActivity.class);
     * // Closing all the Activities
     * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
     * <p>
     * // Add new Flag to start new Activity
     * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     * <p>
     * // Staring Login Activity
     * _context.startActivity(i);
     * <p>
     * }
     */

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);

    }

    // Getter & Setter untuk mengambil data session
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getUsername() {
        return pref.getString(USERNAME, "");
    }

    public void setUsername(String v) {
        editor.putString(USERNAME, v);
        editor.commit();
    }

    public String getFullName() {
        return pref.getString(FULL_NAME, "");
    }

    public void setFullName(String v) {
        editor.putString(FULL_NAME, v);
        editor.commit();
    }

    public String getRole() {
        return pref.getString(ROLE, "");
    }

    public void setRole(String v) {
        editor.putString(ROLE, v);
        editor.commit();
    }

    public StudentModel getStudent() {
        String studentStr = pref.getString(STUDENT, "");
        return gson.fromJson(studentStr, StudentModel.class);
    }

    public void setStudent(StudentModel v) {
        String studentStr = gson.toJson(v);
        editor.putString(STUDENT, studentStr);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(TOKEN, "");
    }

    public void setToken(String v) {
        editor.putString(TOKEN, v);
        editor.commit();
    }

}
