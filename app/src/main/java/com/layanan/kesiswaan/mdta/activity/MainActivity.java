package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.Features.CLASSROOM;
import static com.layanan.kesiswaan.mdta.enums.Features.GRADES;
import static com.layanan.kesiswaan.mdta.enums.Features.NEWS;
import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT;
import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT_ATTENDANCE;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.CLASSROOM_ID;
import static com.layanan.kesiswaan.mdta.utils.Constants.DATE;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_TITLE;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;
import static com.layanan.kesiswaan.mdta.utils.Constants.SEMESTER;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.enums.Role;
import com.layanan.kesiswaan.mdta.helpers.CommonHelper;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.helpers.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends BaseActivity {
    @BindView(R.id.layout)
    CoordinatorLayout layout;
    @BindView(R.id.cvNews)
    CardView cvNews;
    @BindView(R.id.cvAttendance)
    CardView cvAttendance;
    @BindView(R.id.cvGrades)
    CardView cvGrades;
    @BindView(R.id.cvAdministration)
    CardView cvAdministration;

    @BindView(R.id.tvNews)
    TextView tvNews;

    @BindView(R.id.btnLoginMain)
    Button btnLogin;
    @BindView(R.id.btnLogoutMain)
    Button btnLogout;

    @BindView(R.id.fabMain)
    FloatingActionMenu fabMain;
    @BindView(R.id.fabUser)
    FloatingActionButton fabUser;
    @BindView(R.id.fabStudent)
    FloatingActionButton fabStudent;
    @BindView(R.id.fabClassRoom)
    FloatingActionButton fabClassRoom;
    @BindView(R.id.fabCourse)
    FloatingActionButton fabCourse;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        token = sessionManager.getToken();
        checkParents(sessionManager);
        cvNews.setOnClickListener(v -> {
            String params = RequestHelper.GenerateApiRequestParam(Features.NEWS);
            progressDialog.show();

            Intent i = new Intent(ctx, NewsActivity.class);
            i.putExtra(INTENT_QUERY, params);
            progressDialog.dismiss();
            ctx.startActivity(i);
        });
        cvAttendance.setOnClickListener(v -> FilterDialog(ctx, progressDialog, Features.STUDENT_ATTENDANCE));
        cvGrades.setOnClickListener(v -> FilterDialog(ctx, progressDialog, Features.GRADES));
        cvAdministration.setOnClickListener(v -> FilterDialog(ctx, progressDialog, Features.ADMINISTRATION));

        btnLogin.setOnClickListener(v -> startActivity(new Intent(ctx, LoginActivity.class)));
        btnLogout.setOnClickListener(v -> logoutDialog());

        fabUser.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertUserActivity.class)));
        fabStudent.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertStudentActivity.class)));
        fabClassRoom.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertClassroomActivity.class)));
        fabCourse.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertCourseActivity.class)));

        if (sessionManager.isLoggedIn()) {
            fabMain.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);
        }
    }

    private void checkParents(SessionManager sessionManager) {
        String userRole = sessionManager.getRole();
        if(userRole.equals(Role.PARENTS.toString()) && sessionManager.getStudent() == null) {
            showStudentNotAssigned();
        }
    }

    public void FilterDialog(Context ctx, ProgressDialog progressDialog, Features feature) {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setInverseBackgroundForced(false);
        progressDialog.setMessage(LOADING_TEXT);

        String role = sessionManager.getRole();
        if (role.equals(Role.ADMIN.toString())) {
            showFilterDialog(ctx, progressDialog, feature);
        } else {
            String params = RequestHelper.GenerateApiRequestParam(feature);
//            Request request = RequestHelper.BuildRequest(GET, params, token);
            progressDialog.dismiss();
            Intent i;
            i = new Intent(ctx, feature == NEWS ? NewsActivity.class : feature == STUDENT_ATTENDANCE ? StudentAttendanceActivity.class :
                    feature == GRADES ? GradesActivity.class : AdministrationActivity.class);
            i.putExtra(INTENT_TITLE, sessionManager.getStudent().getFullName());
            i.putExtra(INTENT_QUERY, params);
            ctx.startActivity(i);
        }
    }

    private void showFilterDialog(Context ctx, ProgressDialog progressDialog, Features feature) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        LayoutInflater inflater = getLayoutInflater();
        View attendanceDialog = inflater.inflate(R.layout.filter_dialog, null);

        LinearLayout layoutSemester = attendanceDialog.findViewById(R.id.layoutSemester);
        LinearLayout layoutDate = attendanceDialog.findViewById(R.id.layoutDate);
        Spinner spSemester = attendanceDialog.findViewById(R.id.spSemester);
        Spinner spClassRoom = attendanceDialog.findViewById(R.id.spinnerClassRoom);
        EditText txtDate = attendanceDialog.findViewById(R.id.txtDate);

        alertDialog.setView(attendanceDialog);
        alertDialog.setCancelable(true);
        alertDialog.setTitle("Filter");
        alertDialog.setPositiveButton("SUBMIT", (dialog, which) -> {
            if (feature == Features.GRADES) {
                String classroom = spClassRoom.getSelectedItem().toString();
                String semester = spSemester.getSelectedItem().toString();
                String params = RequestHelper.GenerateApiRequestParam(Features.GRADES, classroom, semester);
                Intent i = new Intent(ctx, GradesActivity.class);
                String titleText = String.format("%s %s %s %s", "Kelas", classroom, "Semester", semester);
                i.putExtra(INTENT_TITLE, titleText);
                i.putExtra(INTENT_QUERY, params);
                ctx.startActivity(i);
            } else if (feature == Features.STUDENT_ATTENDANCE) {
                txtDate.setError(null);

                boolean cancel = false;
                View focusView = null;

                String cannot_empty = getResources().getString(R.string.cannotEmpty);
                String date = getResources().getString(R.string.date);
                String errMessageDate = String.format("%s %s", date, cannot_empty);
                if (isEmpty(txtDate)) {
                    txtDate.setError(errMessageDate);
                    focusView = txtDate;
                    cancel = true;
                }
                if (cancel) {
                    Toast.makeText(ctx, errMessageDate, Toast.LENGTH_SHORT).show();
                    focusView.requestFocus();
                } else {
//                    String classroom = spClassRoom.getSelectedItem().toString();
                    String dateData = txtDate.getText().toString();
                    String params = RequestHelper.GenerateApiRequestParam(Features.STUDENT_ATTENDANCE, dateData);
                    Intent i = new Intent(ctx, StudentAttendanceActivity.class);
//                            i.putExtra(CLASSROOM_ID, classroom);
                    i.putExtra(DATE, dateData);
                    i.putExtra(INTENT_QUERY, params);
                    ctx.startActivity(i);
                }
            } else if (feature == Features.ADMINISTRATION) {
                String classroom = spClassRoom.getSelectedItem().toString();
                String params = RequestHelper.GenerateApiRequestParam(Features.ADMINISTRATION, classroom);
                Intent i = new Intent(ctx, AdministrationActivity.class);
                i.putExtra(INTENT_TITLE, classroom);
                i.putExtra(INTENT_QUERY, params);
                ctx.startActivity(i);
            } else {
//                String params = RequestHelper.GenerateApiRequestParam(Features.NEWS);
//                progressDialog.show();
//
//                Intent i = new Intent(ctx, NewsActivity.class);
//                i.putExtra(INTENT_QUERY, params);
//                progressDialog.dismiss();
//                ctx.startActivity(i);
            }
            dialog.dismiss();
        });

        alertDialog.setNegativeButton("CANCEL", (dialog, which) -> dialog.dismiss());
        alertDialog.show();

        if (feature == Features.STUDENT_ATTENDANCE) layoutSemester.setVisibility(View.GONE);
        if (feature == Features.GRADES) layoutDate.setVisibility(View.GONE);
        if (feature == Features.ADMINISTRATION) {
            layoutDate.setVisibility(View.GONE);
            layoutSemester.setVisibility(View.GONE);
        }

//        String classroomParams = RequestHelper.GenerateApiRequestParam(CLASSROOM, sessionManager.getToken());
//        Request classroomRequest = RequestHelper.BuildRequest(GET, classroomParams);
//        logger(classroomRequest);

        String[] semesterData = getResources().getStringArray(R.array.semester);
        CommonHelper commonHelper = new CommonHelper();
        loadSpinnerCallApi(ctx, progressDialog, spClassRoom, "", CLASSROOM);
        commonHelper.loadSpinner(ctx, spSemester, semesterData, "");

        txtDate.setOnClickListener(v -> {
            commonHelper.showDatePickerDialog(ctx, txtDate);
        });
    }

    private void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
//        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(LOADING_TEXT);

        dialog.show();
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String params = RequestHelper.GenerateApiRequestParam(features);
        Request request = RequestHelper.BuildRequest(GET, params, token);
        logger(request);
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
                String message = e.getMessage();
                loggerError(message);
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                runOnUiThread(() -> {
                    try {
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                        String res = response.body().string();
                        List<String> listSpinnerData = new ArrayList<>();
                        if (response.isSuccessful()) {
                            JSONArray jsonArray = new JSONArray(res);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String id = "";
                                if (features == STUDENT) {
                                    id = jsonObject.getString("username");
                                } else {
                                    id = jsonObject.getString("id");
                                }
                                listSpinnerData.add(id);
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                                    listSpinnerData);
                            spinner.setAdapter(adapter);
                            if (selectedPosition != null)
                                spinner.setSelection(adapter.getPosition(selectedPosition));
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(res);
                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        dialog.dismiss();
                        String message = e.getMessage();
                        loggerError(message);
                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
