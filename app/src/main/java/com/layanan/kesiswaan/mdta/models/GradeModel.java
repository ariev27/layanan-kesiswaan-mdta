package com.layanan.kesiswaan.mdta.models;

import java.io.Serializable;

public class GradeModel implements Serializable {
    private String id, semester, points, createdAt, updatedAt;
    private StudentModel student;
    private CourseModel course;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public StudentModel getStudent() {
        return student;
    }

    public void setStudent(StudentModel student) {
        this.student = student;
    }

    public CourseModel getCourse() {
        return course;
    }

    public void setCourse(CourseModel course) {
        this.course = course;
    }

//    public boolean isEmpty()  {
//
//        for (Field field : this.getClass().getDeclaredFields()) {
//            try {
//                field.setAccessible(true);
//                if (field.get(this)!=null) {
//                    return false;
//                }
//            } catch (Exception e) {
//                System.out.println("Exception occured in processing");
//            }
//        }
//        return true;
//    }
}
