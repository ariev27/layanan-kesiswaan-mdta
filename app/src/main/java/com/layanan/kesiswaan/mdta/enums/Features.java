package com.layanan.kesiswaan.mdta.enums;

public enum Features {
    NEWS, STUDENT_ATTENDANCE, GRADES, ADMINISTRATION, LOGIN, USER,
    CLASSROOM, STUDENT, COURSE
}
