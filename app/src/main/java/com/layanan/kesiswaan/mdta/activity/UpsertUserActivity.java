package com.layanan.kesiswaan.mdta.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;

public class UpsertUserActivity extends BaseActivity {
    @BindView(R.id.txtFullName)
    TextInputEditText txtFullName;
    @BindView(R.id.txtUsername)
    TextInputEditText txtUsername;
    @BindView(R.id.txtPassword)
    TextInputEditText txtPassword;

    @BindView(R.id.btnSubmitUser)
    Button btnSubmitUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);

        btnSubmitUser.setOnClickListener(v -> createNewUser());
    }

    private void createNewUser() {
        txtFullName.setError(null);
        txtUsername.setError(null);
        txtPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageFullName = String.format("%s %s", "Full name", cannot_empty);
        String errMessageUsername = String.format("%s %s", "Username", cannot_empty);
        String errMessagePassword = String.format("%s %s", "Password", cannot_empty);
        if (isEmpty(txtFullName)) {
            txtFullName.setError(errMessageFullName);
            focusView = txtFullName;
            cancel = true;
        } else if (isEmpty(txtUsername)) {
            txtUsername.setError(errMessageUsername);
            focusView = txtUsername;
            cancel = true;
        } else if (isEmpty(txtPassword)) {
            txtPassword.setError(errMessagePassword);
            focusView = txtPassword;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // Set data yang akan di kirim dengan method POST
            RequestBody requestBody = new FormBody.Builder()
                    .add("fullName", txtFullName.getText().toString())
                    .add("username", txtUsername.getText().toString())
                    .add("password", txtPassword.getText().toString())
                    .build();

            String params = RequestHelper.GenerateApiRequestParam(Features.USER);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            logger(params);
            logger(request);

            // Munculkan animasi loading dengan memanggil method showLoading
            showLoading();

            // Menangani callback
            httpClient.newCall(request).enqueue(new okhttp3.Callback() {

                // Jika gagal
                @Override
                public void onFailure(Call call, final IOException e) {
                    loggerError(e);
                    hideLoading();
                }

                // Jika ada respon
                @Override
                public void onResponse(Call call, Response response) {
                    logger("RES" + response);
                    runOnUiThread(() -> {
                        try {
                            hideLoading();
                            // Inisiasi variabel responData dan rubah ke bentuk string
                            String res = response.body().string();
                            if (!response.isSuccessful()) {
                                // Inisiasi variabel json dan isi dengan responData
                                JSONObject jsonObject = new JSONObject(res);
                                // Inisiasi variabel json dan isi dengan responData
                                Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                logger("RESBRO " + res);
                                Toast.makeText(ctx, res, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | IOException e) {
                            loggerError((JSONException) e);
                        }
                    });
                }
            });
        }
    }
}