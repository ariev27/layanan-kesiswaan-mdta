package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.helpers.SessionManager;
import com.layanan.kesiswaan.mdta.models.ErrorModel;
import com.layanan.kesiswaan.mdta.models.LoginModel;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class LoginActivity extends BaseActivity {
    @BindView(R.id.loginLayout)
    RelativeLayout layout;

    @BindView(R.id.txtUsername)
    TextInputEditText txtUsername;
    @BindView(R.id.txtPassword)
    TextInputEditText txtPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(ctx, MainActivity.class));
            finish();
        }
        // Bila btnLogin di klik akan melakukan prosedur login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method actionLogin
                actionLogin();
            }
        });

    }

    // Method untuk login
    private void actionLogin() {
        txtUsername.setError(null);
        txtPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Ketika txtUsername atau txtPassword tidak di isi atau kosong
        // Maka akan menampilkan pesan error

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageUsername = String.format("%s %s", "Username", cannot_empty);
        String errMessagePassword = String.format("%s %s", "Password", cannot_empty);
        if (isEmpty(txtUsername)) {
            txtUsername.setError(errMessageUsername);
            focusView = txtUsername;
            cancel = true;
        } else if (isEmpty(txtPassword)) {
            txtPassword.setError(errMessagePassword);
            focusView = txtPassword;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            // Set data yang akan di kirim dengan method POST
            RequestBody requestBody = new FormBody.Builder()
                    .add("username", txtUsername.getText().toString())
                    .add("password", txtPassword.getText().toString())
                    .build();

            String params = RequestHelper.GenerateRequestParam(Features.LOGIN);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody);
            logger(params);
            logger(request);

            // Munculkan animasi loading dengan memanggil method showLoading
            showLoading();

            // Menangani callback
            httpClient.newCall(request).enqueue(new okhttp3.Callback() {

                // Jika gagal
                @Override
                public void onFailure(Call call, final IOException e) {
                    loggerError(e);
                    hideLoading();
                }

                // Jika ada respon
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    logger("RES" + response);
                    hideLoading();
                    String responseStr = response.body().string();
                    if (!response.isSuccessful()) {
                        ErrorModel errorModel = gson.fromJson(responseStr, ErrorModel.class);
                        Snackbar.make(layout, errorModel.getMessage(), Snackbar.LENGTH_SHORT).show();
                    } else {
                        LoginModel loginResponse = gson.fromJson(responseStr, LoginModel.class);
                        SessionManager sessionManager = new SessionManager(ctx);
                        sessionManager.createLoginSession(
                                loginResponse.getUsername(),
                                loginResponse.getFullName(),
                                loginResponse.getRole(),
                                loginResponse.getStudent(),
                                loginResponse.getToken()
                        );

                        //Arahkan ke halaman main
                        startActivity(new Intent(ctx, MainActivity.class));
                        finish();
                    }
                }
            });
        }
    }

}