package com.layanan.kesiswaan.mdta.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.activity.UpsertNewsActivity;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.helpers.SessionManager;
import com.layanan.kesiswaan.mdta.models.NewsModel;

import java.io.Serializable;
import java.util.ArrayList;

import okhttp3.Request;

import static com.layanan.kesiswaan.mdta.enums.Features.NEWS;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.DELETE;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context ctx;
    private final ArrayList<NewsModel> newsModelList;
    private ViewGroup viewGroup;

    public NewsAdapter(Context ctx, ArrayList<NewsModel> newsModelList, ViewGroup viewGroup) {
        this.ctx = ctx;
        this.newsModelList = newsModelList;
        this.viewGroup = viewGroup;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.news_list, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final NewsModel newsModel = newsModelList.get(position);
        NewsViewHolder newsViewHolder = (NewsViewHolder) holder;

        newsViewHolder.tvTitle.setText(newsModel.getTitle());
        newsViewHolder.tvDescription.setText(newsModel.getDescription());
        newsViewHolder.tvCreatedBy.setText(newsModel.getCreatedBy());
        newsViewHolder.tvCreatedAt.setText(newsModel.getCreatedAt());
        newsViewHolder.tvUpdatedAt.setText(newsModel.getUpdatedAt());

        newsViewHolder.newsContainer.setOnClickListener(v -> {
            // TODO view detail
        });

        newsViewHolder.newsContainer.setOnLongClickListener(v -> {
            // TODO edit or delete
            String newsQuery = RequestHelper.GenerateRequestParam(NEWS);
            optionDialog(NEWS, String.valueOf(newsModel.getId()), newsQuery, newsModel, UpsertNewsActivity.class);
            return true;
        });

    }

    private void optionDialog(Features feature, String id, String queryParam, Serializable data, Class<? extends Activity> activity) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setTitle("Options")
                .setItems(R.array.options, (dialog1, which) -> {
                    RequestHelper requestHelper = new RequestHelper();
                    if (which == 0) {
                        //Edit
                        Intent i = new Intent(ctx, activity);
                        i.putExtra(INTENT_DATA, data);
                        i.putExtra(INTENT_QUERY, queryParam);
                        ctx.startActivity(i);

                    } else {
                        //Delete
                        SessionManager sessionManager = new SessionManager(ctx);
                        ProgressDialog progressDialog = new ProgressDialog(ctx);
                        String params = RequestHelper.GenerateDeleteRequestParam(feature, id);
                        Request request = RequestHelper.BuildRequest(DELETE, params, sessionManager.getToken());
                        requestHelper.createCommonRequest(viewGroup, progressDialog, request);
                    }
                });
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return newsModelList == null ? 0 : newsModelList.size();
    }

    private static class NewsViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout newsContainer;
        TextView tvTitle, tvDescription, tvCreatedBy, tvCreatedAt, tvUpdatedAt;

        NewsViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvCreatedBy = view.findViewById(R.id.tvCreatedBy);
            tvCreatedAt = view.findViewById(R.id.tvCreatedAt);
            tvUpdatedAt = view.findViewById(R.id.tvUpdatedAt);
            newsContainer = view.findViewById(R.id.newsContainer);
        }
    }
}
