package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.Features.GRADES;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.CLASSROOM_ID;
import static com.layanan.kesiswaan.mdta.utils.Constants.DATA_NOT_FOUND;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_TITLE;
import static com.layanan.kesiswaan.mdta.utils.Constants.SEMESTER;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.GradeModel;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class GradesActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.tableView)
    TableLayout tableLayout;
    @BindView(R.id.swLayout)
    SwipeRefreshLayout swLayout;

    @BindView(R.id.fabGrades)
    FloatingActionButton fabGrades;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
            TableLayout.LayoutParams.WRAP_CONTENT);
    String titleText;
    String gradesQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ButterKnife.bind(this);

        if (sessionManager.isLoggedIn()) {
            fabGrades.setVisibility(View.VISIBLE);
        }

        fabGrades.setOnClickListener(v -> startActivity(new Intent(ctx, UpsertGradesActivity.class)));

        titleText = getIntent().getStringExtra(INTENT_TITLE);
        gradesQuery = getIntent().getStringExtra(INTENT_QUERY);
        tableLayout.setStretchAllColumns(true);

        String title = String.format("%s %s", getResources().getString(R.string.gradesList), titleText);
        tvTitle.setText(title);

        loadData(gradesQuery, progressDialog);

        // Mengeset listener yang akan dijalankan saat layar di refresh/swipe
        swLayout.setOnRefreshListener(() -> new Handler().postDelayed(() -> {
            loadData(gradesQuery, progressDialog);
            swLayout.setRefreshing(false);
        }, 2000));
    }

    private void loadData(String gradesQuery, ProgressDialog progressDialog) {
        Request request = RequestHelper.BuildRequest(GET, gradesQuery, sessionManager.getToken());
        progressDialog.show();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                String errMessage = e.getMessage();
                loggerError(errMessage);
                Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                String responseStr = response.body().string();
                GradeModel[] gradeList = gson.fromJson(responseStr, GradeModel[].class);
                renderTable(ctx, gradeList, tableLayout, progressDialog);
            }
        });
    }

    void renderTable(Context ctx, GradeModel[] gradeData, TableLayout tableLayout, ProgressDialog progressDialog) {
        runOnUiThread(() -> {
            trParams.setMargins(0, 0, 0, 0);
            tableLayout.removeAllViews();

            createTableHeader(ctx, tableLayout);
            createTableBody(ctx, gradeData, progressDialog);
        });
    }

    void createTableHeader(Context ctx, TableLayout tableLayout) {
        int primaryColor = getResources().getColor(R.color.colorPrimary);

        TableRow trHeader = new TableRow(ctx);
        trHeader.setBackgroundColor(primaryColor);
        TextView tvHeader1 = new TextView(ctx);
        TextView tvHeader2 = new TextView(ctx);
        TextView tvHeader3 = new TextView(ctx);
        TextView tvHeader4 = new TextView(ctx);
        TextView tvHeader5 = new TextView(ctx);
        TextView tvHeader6 = new TextView(ctx);

        tvHeader1.setTextColor(Color.WHITE);
        tvHeader2.setTextColor(Color.WHITE);
        tvHeader3.setTextColor(Color.WHITE);
        tvHeader4.setTextColor(Color.WHITE);
        tvHeader5.setTextColor(Color.WHITE);
        tvHeader6.setTextColor(Color.WHITE);

        tvHeader1.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader2.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader3.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader4.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader5.setGravity(Gravity.CENTER_HORIZONTAL);
        tvHeader6.setGravity(Gravity.CENTER_HORIZONTAL);

        tvHeader1.setPadding(10, 10, 10, 10);
        tvHeader2.setPadding(10, 10, 10, 10);
        tvHeader3.setPadding(10, 10, 10, 10);
        tvHeader4.setPadding(10, 10, 10, 10);
        tvHeader5.setPadding(10, 10, 10, 10);
        tvHeader6.setPadding(10, 10, 10, 10);

        tvHeader1.setTypeface(null, Typeface.BOLD);
        tvHeader2.setTypeface(null, Typeface.BOLD);
        tvHeader3.setTypeface(null, Typeface.BOLD);
        tvHeader4.setTypeface(null, Typeface.BOLD);
        tvHeader5.setTypeface(null, Typeface.BOLD);
        tvHeader6.setTypeface(null, Typeface.BOLD);

        tvHeader1.setBackgroundColor(primaryColor);
        tvHeader2.setBackgroundColor(primaryColor);
        tvHeader3.setBackgroundColor(primaryColor);
        tvHeader4.setBackgroundColor(primaryColor);
        tvHeader5.setBackgroundColor(primaryColor);
        tvHeader6.setBackgroundColor(primaryColor);

        tvHeader1.setText(getResources().getString(R.string.no));
        tvHeader2.setText(getResources().getString(R.string.studentNameCaps));
        tvHeader3.setText(getResources().getString(R.string.classroomCaps));
        tvHeader4.setText(getResources().getString(R.string.semesterCaps));
        tvHeader5.setText(getResources().getString(R.string.courseCaps));
        tvHeader6.setText(getResources().getString(R.string.pointsCaps));

        trHeader.addView(tvHeader1);
        trHeader.addView(tvHeader2);
        trHeader.addView(tvHeader3);
        trHeader.addView(tvHeader4);
        trHeader.addView(tvHeader5);
        trHeader.addView(tvHeader6);

        tableLayout.addView(trHeader, trParams);
    }

    void createTableBody(Context ctx, GradeModel[] gradeModels, ProgressDialog progressDialog) {
        int primaryColor = getResources().getColor(R.color.colorPrimary);
        int primaryColorLight = getResources().getColor(R.color.colorPrimaryLight);

        if (gradeModels.length == 0) {
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT);
            TextView tvBody = new TextView(ctx);
            TableRow trBody = new TableRow(ctx);

            trBody.setPadding(0, 0, 0, 0);
            trBody.setLayoutParams(trParams);

            tvBody.setText(DATA_NOT_FOUND);
            tvBody.setTextColor(Color.WHITE);
            tvBody.setGravity(Gravity.CENTER_HORIZONTAL);
            tvBody.setTypeface(null, Typeface.BOLD);
            tvBody.setAllCaps(true);
            tvBody.setPadding(10, 30, 10, 30);

            trBody.setBackgroundColor(primaryColorLight);
            trBody.setGravity(Gravity.CENTER_HORIZONTAL);
            trBody.addView(tvBody);
            tableLayout.addView(trBody, trParams);
        } else {
            for (int i = 0; i < gradeModels.length; i++) {
                GradeModel gradeModel = gradeModels[i];
                TextView tvBody1 = new TextView(ctx);
                TextView tvBody2 = new TextView(ctx);
                TextView tvBody3 = new TextView(ctx);
                TextView tvBody4 = new TextView(ctx);
                TextView tvBody5 = new TextView(ctx);
                TextView tvBody6 = new TextView(ctx);

                tvBody1.setText(String.valueOf(i + 1));
                tvBody2.setText(gradeModel.getStudent().getFullName());
                tvBody3.setText(gradeModel.getStudent().getClassroomId());
                tvBody4.setText(gradeModel.getSemester());
                tvBody5.setText(gradeModel.getCourse().getName());
                tvBody6.setText(gradeModel.getPoints());

                tvBody1.setTextColor(Color.WHITE);
                tvBody2.setTextColor(Color.WHITE);
                tvBody3.setTextColor(Color.WHITE);
                tvBody4.setTextColor(Color.WHITE);
                tvBody5.setTextColor(Color.WHITE);
                tvBody6.setTextColor(Color.WHITE);

                tvBody1.setBackgroundColor(primaryColor);
                tvBody2.setBackgroundColor(primaryColorLight);
                tvBody3.setBackgroundColor(primaryColor);
                tvBody4.setBackgroundColor(primaryColorLight);
                tvBody5.setBackgroundColor(primaryColor);
                tvBody6.setBackgroundColor(primaryColorLight);

                tvBody1.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody2.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody3.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody4.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody5.setGravity(Gravity.CENTER_HORIZONTAL);
                tvBody6.setGravity(Gravity.CENTER_HORIZONTAL);

                tvBody1.setPadding(10, 30, 10, 30);
                tvBody2.setPadding(10, 30, 10, 30);
                tvBody3.setPadding(10, 30, 10, 30);
                tvBody4.setPadding(10, 30, 10, 30);
                tvBody5.setPadding(10, 30, 10, 30);
                tvBody6.setPadding(10, 30, 10, 30);

                TableRow trBody = new TableRow(ctx);
                trBody.setPadding(0, 0, 0, 0);
                trBody.setLayoutParams(trParams);

                trBody.addView(tvBody1);
                trBody.addView(tvBody2);
                trBody.addView(tvBody3);
                trBody.addView(tvBody4);
                trBody.addView(tvBody5);
                trBody.addView(tvBody6);

                if (sessionManager.isLoggedIn()) {
                    trBody.setOnLongClickListener(v -> {
                        optionDialog(GRADES, gradeModel.getId(), gradesQuery, gradeModel, UpsertGradesActivity.class, layout);
                        return false;
                    });
                }

                tableLayout.addView(trBody, trParams);
            }
        }
    }

    @Override
    protected void onDestroy() {
        progressDialog.dismiss();
        super.onDestroy();
    }
}
