package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.reflect.TypeToken;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.adapters.NewsAdapter;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.ErrorModel;
import com.layanan.kesiswaan.mdta.models.NewsModel;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class NewsActivity extends BaseActivity {
    @BindView(R.id.newsLayout)
    LinearLayout layout;

    @BindView(R.id.rvNews)
    RecyclerView rvNews;
    @BindView(R.id.swLayout)
    SwipeRefreshLayout swLayout;

    @BindView(R.id.fabNews)
    FloatingActionButton fabNews;

    private String newsQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        newsQuery = getIntent().getStringExtra(INTENT_QUERY);

        if (sessionManager.isLoggedIn()) {
            fabNews.setVisibility(View.VISIBLE);
        }

        fabNews.setOnClickListener(v -> {
            Intent i = new Intent(ctx, UpsertNewsActivity.class);
            i.putExtra(INTENT_QUERY, newsQuery);
            ctx.startActivity(i);
        });

        loadData(newsQuery, progressDialog);

        swLayout.setOnRefreshListener(() -> new Handler().postDelayed(() -> {
            loadData(newsQuery, progressDialog);
            swLayout.setRefreshing(false);
        }, 2000));
    }

    private void loadData(String newsQuery, ProgressDialog progressDialog) {
        Request request = RequestHelper.BuildRequest(GET, newsQuery, sessionManager.getToken());
        progressDialog.show();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                String errMessage = e.getMessage();
                loggerError(errMessage);
                Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                if (!response.isSuccessful()) {
                    progressDialog.dismiss();
                    ErrorModel errorModel = gson.fromJson(responseStr, ErrorModel.class);
                    Snackbar.make(layout, errorModel.getMessage(), Snackbar.LENGTH_SHORT).show();
                } else {
                    ArrayList<NewsModel> newsList = gson.fromJson(responseStr, new TypeToken<ArrayList<NewsModel>>() {
                    }.getType());
                    progressDialog.dismiss();
                    loadRecyclerView(ctx, rvNews, newsList);
                }
            }
        });
    }

    private void loadRecyclerView(Context ctx, RecyclerView rvNews, ArrayList<NewsModel> newsModels) {
        runOnUiThread(() -> {
            rvNews.setLayoutManager(new LinearLayoutManager(ctx));
            NewsAdapter newsAdapter = new NewsAdapter(ctx, newsModels, layout);
            rvNews.setAdapter(newsAdapter);
        });
    }
}