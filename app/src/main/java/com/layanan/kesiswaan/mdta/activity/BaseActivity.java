package com.layanan.kesiswaan.mdta.activity;

import static com.layanan.kesiswaan.mdta.enums.HttpMethod.DELETE;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;
import static com.layanan.kesiswaan.mdta.utils.Constants.STUDENT_NOT_SET;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.helpers.SessionManager;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class BaseActivity extends AppCompatActivity {

    protected Context ctx;
    protected AlphaAnimation btnAnimasi = new AlphaAnimation(1F, 0.7F);
    protected TextView tvTitle;
    protected static OkHttpClient httpClient;
    protected ProgressDialog progressDialog;
    protected SessionManager sessionManager;
    protected static Snackbar snackbar;
    protected Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ctx = this;
        sessionManager = new SessionManager(ctx);
        progressDialog = new ProgressDialog(ctx);

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

    }

    protected void showLoading() {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setInverseBackgroundForced(false);
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setMessage(LOADING_TEXT);

        progressDialog.show();
    }

    protected void showStudentNotAssigned() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setMessage(STUDENT_NOT_SET)
                .setTitle("Student Not Set")
                .setCancelable(false)
                .setPositiveButton("Logout", (dialog, id) -> sessionManager.logoutUser())
                .show();
    }

    protected void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    protected void logoutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setMessage("Anda yakin mau logout ?");
        alertDialog.setTitle("Konfirmasi");
        alertDialog.setPositiveButton("Ya", (dialog, id) -> {
            sessionManager.logoutUser();
        });
        alertDialog.setNegativeButton("Tidak", (dialog, id) -> dialog.cancel());
        alertDialog.show();
    }

    protected void optionDialog(Features feature, String id, String queryParam, Serializable data, Class<? extends Activity> activity, LinearLayout layout) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setTitle("Options")
                .setItems(R.array.options, (dialog1, which) -> {
                    RequestHelper requestHelper = new RequestHelper();
                    if (which == 0) {
                        //Edit
                        Intent i = new Intent(ctx, activity);
                        i.putExtra(INTENT_DATA, data);
                        i.putExtra(INTENT_QUERY, queryParam);
                        startActivity(i);
                    } else {
                        //Delete
                        String params = RequestHelper.GenerateDeleteRequestParam(feature, id);
                        Request request = RequestHelper.BuildRequest(DELETE, params, sessionManager.getToken());
                        requestHelper.createCommonRequest(layout, progressDialog, request);
                    }
                });
        alertDialog.show();
    }

//    protected void refreshTable(Context ctx, String query, Activity activity) {
//        Intent i = new Intent(ctx, activity.getClass());
//        i.putExtra(CLASSROOM_ID, res);
//        i.putExtra(INTENT_QUERY, query);
//        ctx.startActivity(i);
//        finish();
//    }

    protected void notificationDialog(String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setTitle(title)
                .setMessage(message)
                .show();
    }

}