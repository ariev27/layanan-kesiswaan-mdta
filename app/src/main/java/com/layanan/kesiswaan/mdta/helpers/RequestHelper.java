package com.layanan.kesiswaan.mdta.helpers;

import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.ADMINISTRATION_PATH;
import static com.layanan.kesiswaan.mdta.utils.Constants.ATTENDANCE_PATH;
import static com.layanan.kesiswaan.mdta.utils.Constants.BASE_API_URL;
import static com.layanan.kesiswaan.mdta.utils.Constants.GRADES_PATH;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;

import android.app.ProgressDialog;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;
import com.layanan.kesiswaan.mdta.activity.BaseActivity;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.enums.HttpMethod;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestHelper extends BaseActivity {

    public static Request BuildRequest(HttpMethod httpMethod, String url) {
        return new Request.Builder()
                .url(url)
                .method(String.valueOf(httpMethod), null)
                .build();
    }

    public static Request BuildRequest(HttpMethod httpMethod, String url, RequestBody requestBody) {
        return new Request.Builder()
                .url(url)
                .method(String.valueOf(httpMethod), requestBody)
                .build();
    }

    public static Request BuildRequest(HttpMethod httpMethod, String url, String token) {
        return new Request.Builder()
                .url(url)
                .method(String.valueOf(httpMethod), null)
                .header("token", token)
                .build();
    }

    public static Request BuildRequest(HttpMethod httpMethod, String url, RequestBody requestBody, String token) {
        return new Request.Builder()
                .url(url)
                .method(String.valueOf(httpMethod), requestBody)
                .header("token", token)
                .build();
    }

    public static String GenerateApiRequestParam(Features feature) {
        String s = feature.toString().toLowerCase().replaceAll("_", "-");
        return String.format("%s%s%s", BASE_API_URL, "/api/", s);
    }

    public static String GenerateApiRequestParam(Features feature, String param1) {
//        String s = feature.toString().toLowerCase().replaceAll("_", "-");
//        return String.format("%s%s%s", BASE_API_URL, "/api/", s);
        switch (feature) {
            case ADMINISTRATION:
                return String.format("%s%s%s%s%s", BASE_API_URL, "/api/", ADMINISTRATION_PATH, "?classroomId=", param1);
            case GRADES:
                return String.format("%s%s%s%s%s", BASE_API_URL, "/api/", GRADES_PATH, "?semester=", param1);
            case STUDENT_ATTENDANCE:
                return String.format("%s%s%s%s%s", BASE_API_URL, "/api/", ATTENDANCE_PATH, "&date=", param1);
            default:
                return String.format("%s%s%s", BASE_API_URL, "/", feature.toString().toLowerCase());
        }
    }

    public static String GenerateApiRequestParam(Features feature, String param1, String param2) {
//        String s = feature.toString().toLowerCase().replaceAll("_", "-");
//        return String.format("%s%s%s", BASE_API_URL, "/api/", s);
        switch (feature) {
            case GRADES:
                return String.format("%s%s%s%s%s%s%s", BASE_API_URL, "/api/", GRADES_PATH, "?classroomId=", param1, "&semester=", param2);
            case STUDENT_ATTENDANCE:
                return String.format("%s%s%s%s%s%s%s", BASE_API_URL, "/api/", ATTENDANCE_PATH, "?classroomId=", param1, "&date=", param2);
            default:
                return String.format("%s%s%s", BASE_API_URL, "/", feature.toString().toLowerCase());
        }
    }

    public static String GenerateRequestParam(Features feature) {
        String s = feature.toString().toLowerCase().replaceAll("_", "-");
        return String.format("%s%s%s", BASE_API_URL, "/", s);
    }

//    public static String GenerateRequestParam(Features feature, String param1) {
//        switch (feature) {
//            case ADMINISTRATION:
//                return String.format("%s%s%s%s", BASE_API_URL, ADMINISTRATION_PATH, "?classroomId=", param1);
//            default:
//                return String.format("%s%s%s", BASE_API_URL, "/", feature.toString().toLowerCase());
//        }
//    }
//
//    public static String GenerateRequestParam(Features feature, String param1, String param2) {
//        switch (feature) {
//            case GRADES:
//                return String.format("%s%s%s%s%s%s", BASE_API_URL, GRADES_PATH, "?classroomId=", param1, "&semester=", param2);
//            case STUDENT_ATTENDANCE:
//                return String.format("%s%s%s%s%s%s", BASE_API_URL, ATTENDANCE_PATH, "?classroomId=", param1, "&date=", param2);
//            default:
//                return String.format("%s%s%s", BASE_API_URL, "/", feature.toString().toLowerCase());
//        }
//    }

    public static String GenerateDeleteRequestParam(Features feature, String id) {
        String s = feature.toString().toLowerCase().replaceAll("_", "-") + "/";
        return String.format("%s%s%s%s", BASE_API_URL, "/api/", s, id);
    }

//    public static String CreateRequest(Request request) throws IOException {
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//
//        OkHttpClient httpClient;
//        httpClient = new OkHttpClient.Builder()
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
//
//        Response response = httpClient.newCall(request).execute();
//        return response.body().string();
//    }

    public void createCommonRequest(ViewGroup layout, ProgressDialog progressDialog, Request request) {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setInverseBackgroundForced(false);
        progressDialog.setMessage(LOADING_TEXT);

        progressDialog.show();
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                progressDialog.dismiss();
                String errMessage = e.getMessage();
                loggerError(errMessage);
                Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                Snackbar.make(layout, response.body().string(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }
}
