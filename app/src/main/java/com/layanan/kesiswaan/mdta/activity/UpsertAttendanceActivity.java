package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.CommonHelper;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.StudentAttendanceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;

public class UpsertAttendanceActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.spStudent)
    Spinner spUsername;
    @BindView(R.id.spDescription)
    Spinner spDescription;
    @BindView(R.id.txtDate)
    TextInputEditText txtDate;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    StudentAttendanceModel attendanceData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_student_attendance);
        ButterKnife.bind(this);

        attendanceData = (StudentAttendanceModel) getIntent().getSerializableExtra(INTENT_DATA);
        loadData(attendanceData);

        btnSubmit.setOnClickListener(v -> upsertStudentAttendance(ctx, progressDialog));
    }

    private void loadData(StudentAttendanceModel attendanceData) {
        String[] attendanceDescriptionData = getResources().getStringArray(R.array.attendanceDescription);
        CommonHelper commonHelper = new CommonHelper();
        if (attendanceData == null) {
            txtDate.setOnClickListener(v -> commonHelper.showDatePickerDialog(ctx, txtDate));
            loadSpinnerCallApi(this, progressDialog, spUsername, "", STUDENT);
            commonHelper.loadSpinner(this, spDescription, attendanceDescriptionData, "");
        } else {
            tvTitle.setText(getResources().getString(R.string.updateStudentAttendance));
            String username = attendanceData.getStudent().getUsername();
            String description = attendanceData.getDescription();

            loadSpinnerCallApi(this, progressDialog, spUsername, username, STUDENT);
            commonHelper.loadSpinner(this, spDescription, attendanceDescriptionData, description);

            txtDate.setText(attendanceData.getUpdatedAt());
            txtDate.setEnabled(false);
            spUsername.setEnabled(false);
        }
    }

    private void upsertStudentAttendance(Context ctx, ProgressDialog progressDialog) {
        txtDate.setError(null);

        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String date = getResources().getString(R.string.date);
        String errMessageDate = String.format("%s %s", date, cannot_empty);
        if (isEmpty(txtDate)) {
            txtDate.setError(errMessageDate);
            focusView = txtDate;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody;
            if (attendanceData == null) {
                requestBody = new FormBody.Builder()
                        .add("studentUsername", spUsername.getSelectedItem().toString())
                        .add("date", txtDate.getText().toString())
                        .add("description", spDescription.getSelectedItem().toString())
                        .build();
            } else {
                requestBody = new FormBody.Builder()
                        .add("id", attendanceData.getId())
                        .add("studentUsername", spUsername.getSelectedItem().toString())
                        .add("date", txtDate.getText().toString())
                        .add("description", spDescription.getSelectedItem().toString())
                        .build();
            }
            String params = RequestHelper.GenerateApiRequestParam(Features.STUDENT_ATTENDANCE);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }


    public void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
//        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(LOADING_TEXT);

        dialog.show();
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String params = RequestHelper.GenerateRequestParam(features);
        Request request = RequestHelper.BuildRequest(GET, params);
        logger(request);
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
                String message = e.getMessage();
                loggerError(message);
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                runOnUiThread(() -> {
                    try {
                        String res = response.body().string();
                        List<String> listSpinnerData = new ArrayList<>();
                        if (response.isSuccessful()) {
                            JSONArray jsonArray = new JSONArray(res);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String id = "";
                                if (features == STUDENT) {
                                    id = jsonObject.getString("username");
                                } else {
                                    id = jsonObject.getString("id");
                                }
                                listSpinnerData.add(id);
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                                    listSpinnerData);
                            spinner.setAdapter(adapter);
                            if (selectedPosition != null)
                                spinner.setSelection(adapter.getPosition(selectedPosition));
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(res);
                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        dialog.dismiss();
                        String message = e.getMessage();
                        loggerError(message);
                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        progressDialog.dismiss();
        super.onDestroy();
    }
}