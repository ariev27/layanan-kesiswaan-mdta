package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.CommonHelper;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.GradeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.layanan.kesiswaan.mdta.enums.Features.COURSE;
import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;

public class UpsertGradesActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.spStudent)
    Spinner spStudent;
    @BindView(R.id.spSemester)
    Spinner spSemester;
    @BindView(R.id.spCourse)
    Spinner spCourse;
    @BindView(R.id.txtPoints)
    TextInputEditText txtPoints;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    GradeModel gradeModelData;
    String gradeQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_grades);
        ButterKnife.bind(this);

        gradeModelData = (GradeModel) getIntent().getSerializableExtra(INTENT_DATA);
        gradeQuery = getIntent().getStringExtra(INTENT_QUERY);
        loadData(gradeModelData);

        btnSubmit.setOnClickListener(v -> upsertGrades(ctx, progressDialog, gradeModelData));
    }

    private void loadData(GradeModel gradeModelData) {
        String[] semesterData = getResources().getStringArray(R.array.semester);
        CommonHelper commonHelper = new CommonHelper();
        if (gradeModelData == null) {
            loadSpinnerCallApi(this, progressDialog, spStudent, "", STUDENT);
            loadSpinnerCallApi(this, progressDialog, spCourse, "", COURSE);
            commonHelper.loadSpinner(this, spSemester, semesterData, "");
        } else {
            tvTitle.setText(getResources().getString(R.string.updateGrades));
            String username = gradeModelData.getStudent().getUsername();
            String course = gradeModelData.getCourse().getId();
            String semester = gradeModelData.getSemester();

            loadSpinnerCallApi(this, progressDialog, spStudent, username, STUDENT);
            loadSpinnerCallApi(this, progressDialog, spCourse, course, COURSE);
            commonHelper.loadSpinner(this, spSemester, semesterData, semester);

            txtPoints.setText(gradeModelData.getPoints());

            spStudent.setEnabled(false);
            spCourse.setEnabled(false);
            spSemester.setEnabled(false);
        }
    }

    private void upsertGrades(Context ctx, ProgressDialog progressDialog, GradeModel gradeModelData) {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setInverseBackgroundForced(false);
        progressDialog.setMessage(LOADING_TEXT);

        txtPoints.setError(null);
        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessagePoints = String.format("%s %s", "Points", cannot_empty);
        if (isEmpty(txtPoints)) {
            txtPoints.setError(errMessagePoints);
            focusView = txtPoints;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody;
            if (gradeModelData == null) {
                requestBody = new FormBody.Builder()
                        .add("studentUsername", spStudent.getSelectedItem().toString())
                        .add("semester", spSemester.getSelectedItem().toString())
                        .add("courseId", spCourse.getSelectedItem().toString())
                        .add("points", txtPoints.getText().toString())
                        .build();
            } else {
                requestBody = new FormBody.Builder()
                        .add("id", gradeModelData.getId())
                        .add("points", txtPoints.getText().toString())
                        .build();
            }

            String params = RequestHelper.GenerateApiRequestParam(Features.GRADES);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }

    public void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
//        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(LOADING_TEXT);

        dialog.show();
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String params = RequestHelper.GenerateApiRequestParam(features);
        Request request = RequestHelper.BuildRequest(GET, params, sessionManager.getToken());
        logger(request);
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
                String message = e.getMessage();
                loggerError(message);
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                runOnUiThread(() -> {
                    try {
                        String res = response.body().string();
                        List<String> listSpinnerData = new ArrayList<>();
                        if (response.isSuccessful()) {
                            JSONArray jsonArray = new JSONArray(res);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String id = "";
                                if (features == STUDENT) {
                                    id = jsonObject.getString("username");
                                } else {
                                    id = jsonObject.getString("id");
                                }
                                listSpinnerData.add(id);
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                                    listSpinnerData);
                            spinner.setAdapter(adapter);
                            if (selectedPosition != null)
                                spinner.setSelection(adapter.getPosition(selectedPosition));
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(res);
                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        dialog.dismiss();
                        String message = e.getMessage();
                        loggerError(message);
                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        progressDialog.dismiss();
        super.onDestroy();
    }
}