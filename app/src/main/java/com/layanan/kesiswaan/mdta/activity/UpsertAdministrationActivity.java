package com.layanan.kesiswaan.mdta.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.layanan.kesiswaan.mdta.R;
import com.layanan.kesiswaan.mdta.enums.Features;
import com.layanan.kesiswaan.mdta.helpers.CommonHelper;
import com.layanan.kesiswaan.mdta.helpers.RequestHelper;
import com.layanan.kesiswaan.mdta.models.AdministrationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.layanan.kesiswaan.mdta.enums.Features.ADMINISTRATION;
import static com.layanan.kesiswaan.mdta.enums.Features.CLASSROOM;
import static com.layanan.kesiswaan.mdta.enums.Features.STUDENT;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.GET;
import static com.layanan.kesiswaan.mdta.enums.HttpMethod.POST;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.isEmpty;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.logger;
import static com.layanan.kesiswaan.mdta.helpers.CommonHelper.loggerError;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_DATA;
import static com.layanan.kesiswaan.mdta.utils.Constants.INTENT_QUERY;
import static com.layanan.kesiswaan.mdta.utils.Constants.LOADING_TEXT;

public class UpsertAdministrationActivity extends BaseActivity {
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txtName)
    TextInputEditText txtName;
    @BindView(R.id.spClassroom)
    Spinner spClassroom;
    @BindView(R.id.txtNominal)
    TextInputEditText txtNominal;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    AdministrationModel administrationModelData;
    String administrationQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert_administration);
        ButterKnife.bind(this);

        administrationModelData = (AdministrationModel) getIntent().getSerializableExtra(INTENT_DATA);
        administrationQuery = getIntent().getStringExtra(INTENT_QUERY);
        loadData(administrationModelData);

        btnSubmit.setOnClickListener(v -> upsertAdministration(ctx, progressDialog, administrationModelData));
    }

    private void loadData(AdministrationModel administrationModelData) {
        if (administrationModelData == null) {
            loadSpinnerCallApi(this, progressDialog, spClassroom, "", CLASSROOM);
        } else {
            tvTitle.setText(getResources().getString(R.string.updateAdministration));
            String name = administrationModelData.getName();
            String classroomId = administrationModelData.getClassroomId();
            String nominal = String.valueOf(administrationModelData.getNominal());

            loadSpinnerCallApi(this, progressDialog, spClassroom, classroomId, CLASSROOM);

            txtName.setText(name);
            txtNominal.setText(nominal);
        }
    }

    private void upsertAdministration(Context ctx, ProgressDialog progressDialog, AdministrationModel administrationModelData) {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setInverseBackgroundForced(false);
        progressDialog.setMessage(LOADING_TEXT);

        txtName.setError(null);
        txtNominal.setError(null);
        boolean cancel = false;
        View focusView = null;

        String cannot_empty = getResources().getString(R.string.cannotEmpty);
        String errMessageName = String.format("%s %s", "Name", cannot_empty);
        String errMessageNominal = String.format("%s %s", "Nominal", cannot_empty);
        if (isEmpty(txtName)) {
            txtName.setError(errMessageName);
            focusView = txtName;
            cancel = true;
        } else if (isEmpty(txtNominal)) {
            txtNominal.setError(errMessageNominal);
            focusView = txtNominal;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RequestBody requestBody;
            if (administrationModelData == null) {
                // Create
                requestBody = new FormBody.Builder()
                        .add("name", txtName.getText().toString())
                        .add("classroomId", spClassroom.getSelectedItem().toString())
                        .add("nominal", txtNominal.getText().toString())
                        .build();
            } else {
                // Edit
                requestBody = new FormBody.Builder()
                        .add("id", administrationModelData.getId())
                        .add("name", txtName.getText().toString())
                        .add("classroomId", spClassroom.getSelectedItem().toString())
                        .add("nominal", txtNominal.getText().toString())
                        .build();
            }

            String params = RequestHelper.GenerateApiRequestParam(ADMINISTRATION);
            Request request = RequestHelper.BuildRequest(POST, params, requestBody, sessionManager.getToken());
            RequestHelper requestHelper = new RequestHelper();
            requestHelper.createCommonRequest(layout, progressDialog, request);
        }
    }

    public void loadSpinnerCallApi(Context ctx, ProgressDialog dialog, Spinner spinner, String selectedPosition, Features features) {
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
//        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(LOADING_TEXT);

        dialog.show();
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String params = RequestHelper.GenerateApiRequestParam(features);
        Request request = RequestHelper.BuildRequest(GET, params, sessionManager.getToken());
        logger(request);
        httpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                String errMessage = e.getMessage();
                loggerError(errMessage);
                Snackbar.make(layout, errMessage, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) {
                runOnUiThread(() -> {
                    try {
                        String res = response.body().string();
                        List<String> listSpinnerData = new ArrayList<>();
                        if (response.isSuccessful()) {
                            JSONArray jsonArray = new JSONArray(res);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String id = "";
                                if (features == STUDENT) {
                                    id = jsonObject.getString("username");
                                } else {
                                    id = jsonObject.getString("id");
                                }
                                listSpinnerData.add(id);
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_selectable_list_item,
                                    listSpinnerData);
                            spinner.setAdapter(adapter);
                            if (selectedPosition != null)
                                spinner.setSelection(adapter.getPosition(selectedPosition));
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(res);
                            Toast.makeText(ctx, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        dialog.dismiss();
                        String message = e.getMessage();
                        loggerError(message);
                        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        progressDialog.dismiss();
        super.onDestroy();
    }
}